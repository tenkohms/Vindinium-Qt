#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class NetworkHandler : public QObject
{
    Q_OBJECT
public:
    explicit NetworkHandler(QObject *parent = 0);
    ~NetworkHandler();
    void SendNetworkRequest(QString, QString);
    void SendNetworkRequest(QString, QString, QString);

signals:
    void NetworkData(const QString *);

public slots:

private slots:
    void NetworkReplyFinished(QNetworkReply *);

private:
    QNetworkAccessManager *networkManager_;
};

#endif // NETWORKHANDLER_H
