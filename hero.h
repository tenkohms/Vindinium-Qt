#ifndef HERO_H
#define HERO_H

#include <QObject>
#include <QPair>

class Hero : public QObject
{
    Q_OBJECT
public:
    explicit Hero(int id, QString name, QPair<int, int> position, int life, int gold, QObject *parent = 0);
    int id();
    QString name();
    QPair< int, int > position();
    int life();
    int gold();

signals:

public slots:

private:
    int id_;
    QString name_;
    QPair < int, int > position_;
    int life_;
    int gold_;

};

#endif // HERO_H
