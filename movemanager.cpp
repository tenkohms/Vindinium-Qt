#include "movemanager.h"
#include <QList>
#include <QPair>
#include <QtMath>
#include <QDebug>
MoveManager::MoveManager(BoardHandler *Boardhandler, HeroManager * hm, QObject *parent)
    : QObject(parent)
    , boardHandler_(Boardhandler)
    , heroManager_(hm)
{

}

QString MoveManager::GetMove()
{
    return nextMove_;
}

int MoveManager::Distance( QPair<int, int> first , QPair<int, int> second )
{
    return ( qSqrt( ( qPow( ( first.second + second.second ), 2 ) +  qPow( ( first.first + second.first ), 2 ) ) ) );
}

void MoveManager::BoardReadyToProcess()
{
    nextMove_.clear();
    bool run = false;
    bool hasMove = false;
    QPair< int, int > target;
    //qDebug() << "Processing";

    if ( heroManager_->GetHeroName( boardHandler_->PlayerLocation() ) == "Hai" )
    {
    if ( ( heroManager_->GetHeroHealth( boardHandler_->PlayerLocation() ) < 45) && ( heroManager_->GetHeroMoney( boardHandler_->PlayerLocation() ) > 2) )
    {
        //qDebug() << "Tavern";
        QList < QPair < int, int > > list = boardHandler_->Taverns();
        int distanceToTavern = 99;
        for (int i = 0; i < list.size() ; i++ )
        {
            int distance =  boardHandler_->Steps( boardHandler_->PlayerLocation() , list[i] );
            if ( distance < distanceToTavern)
            {
                target = list[i];
                distanceToTavern = distance;
            }
        }
        hasMove = true;
    }
    }

    //attack/avoid enemy
    if (!hasMove)
    {
        QList< QPair < int, int > > list = boardHandler_->EnemyLocations();

        int enemyChooser = 101;
        for (int i = 0; i < list.size() ; i++)
        {
            int enemyDistance = boardHandler_->Steps( boardHandler_->PlayerLocation(), list[i] );
            //qDebug() << "Enemy at: " << list[i] << " health: " << heroManager_->GetHeroHealth( list[i] );
            //qDebug() << "Distance: " << enemyDistance;
            if ( heroManager_->GetHeroName( boardHandler_->PlayerLocation() ) == "Hai" )
            {
                if ( enemyDistance < 4 )
                {
                    if ( ( heroManager_->GetHeroHealth( list[i] ) < 20) || ( heroManager_->GetHeroHealth( list[i] ) < heroManager_->GetHeroHealth( boardHandler_->PlayerLocation() ) ) )
                    {
                        //qDebug() << "Attack!";
                        run = false;
                    }
                    else
                    {
                        //qDebug() << "Run!";
                        run = true;
                    }
                    target = list[i];
                    //qDebug() << "Enemy: " << heroManager_->GetHeroName( target );
                    hasMove = true;
                    break;
                }
            }
            else
            {
                if ( enemyDistance < enemyChooser )
                {
                    target = list[i];
                    enemyChooser = enemyDistance;
                    //qDebug() << "Enemy: " << heroManager_->GetHeroName( target );
                    hasMove = true;

                }
            }
        }
    }

    if (!hasMove && ( ( ( boardHandler_->PlayerOwnedMineCount() < 1) && heroManager_->GetHeroMoney( boardHandler_->PlayerLocation() ) < 2 ) || ( ( heroManager_->GetHeroHealth( boardHandler_->PlayerLocation() ) > 45 ) ) ) )
    {
         QList< QPair < int, int > > list = boardHandler_->MinesOfInterest();
         if (list.size() > 0)
         {
             int distanceToMine = 99;
             for (int i = 0; i < list.size() ; i++ )
             {
                 int distance = boardHandler_->Steps(boardHandler_->PlayerLocation() , list[i] );
                 if ( distance < distanceToMine )
                 {

                     hasMove = true;
                     target = list[i];
                     distanceToMine = distance;
                 }
             }
             //qDebug() << "mine: " << target;
         }
         else
         {
             QList < QPair < int, int > > tavernList = boardHandler_->Taverns();
             int distanceToTavern = 99;
             for (int i = 0; i < tavernList.size() ; i++ )
             {
                 int distance =  boardHandler_->Steps( boardHandler_->PlayerLocation() , tavernList[i] );
                 if ( distance < distanceToTavern)
                 {
                     target = tavernList[i];
                     distanceToTavern = distance;
                     hasMove = true;
                 }
             }
         }

    }

    if (hasMove)
    {
        switch ( boardHandler_->Dir( boardHandler_->PlayerLocation(), target ) )
        {
        case 0: {
            nextMove_ = ( run ? "South" : "North" );
            break;
        }
        case 1: {
            nextMove_ = ( run ? "North" : "South" );
            break;
        }
        case 2: {
            nextMove_ = ( run ? "West" : "East" );
            break;
        }
        case 3: {
            nextMove_ = ( run ? "East" : "West" );
            break;
        }
        default:
        {
            nextMove_ = "Stay";
            break;
        }

        }
    }
    else
    {
        nextMove_ = "Stay";
    }

    //qDebug() << "emitting: " << nextMove_;
    emit Ready();
}
