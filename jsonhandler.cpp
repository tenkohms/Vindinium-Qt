#include "jsonhandler.h"
#include <QDebug>

JSONHandler::JSONHandler(QString Username, QObject *parent)
    : QObject(parent)
    , username(Username)
{

}

void JSONHandler::NewJSONData(const QString *NetworkData)
{
    QString theMessage = *NetworkData;
    //qDebug() << theMessage;
    QJsonDocument jDoc = QJsonDocument::fromJson(theMessage.toUtf8());
    if (jDoc.isNull() || jDoc.isEmpty())
    {
        qDebug() << "::NewJSONData jDoc null or empty";
        emit NullData();
        return;
    }


    QJsonObject jObj = jDoc.object();

    emit PlayURL(jObj.value("playUrl").toString());

    emit ViewURL(jObj.value("viewUrl").toString());

    emit TotalTurns(jObj.value("game").toObject().value("maxTurns").toDouble());

    emit CurrentTurnCount(jObj.value("game").toObject().value("turn").toDouble());

    QJsonArray heroes = jObj.value("game").toObject().value("heroes").toArray();
    //qDebug() << "Username: " << username;
    for ( int i = 0; i < heroes.size(); i++ )
    {
        if (username != "Hai" )
        {
            //qDebug() << " I am not hai";
            if (heroes[ i ].toObject().value( "name" ).toString() == "Hai")
                emit ProtectPlayer(heroes[ i ].toObject().value( "id" ).toDouble());
        }

        if (heroes[ i ].toObject().value( "name" ).toString() == username)
            emit PlayerID(heroes[ i ].toObject().value( "id" ).toDouble());

        emit NewHero( heroes[ i ].toObject().value( "id" ).toDouble(),
                   heroes[ i ].toObject().value( "name" ).toString(),
                   QPair< int, int >( heroes[ i ].toObject().value( "pos" ).toObject().value( "x" ).toDouble(),
                                      heroes[ i ].toObject().value( "pos" ).toObject().value( "y" ).toDouble()),
                   heroes[ i ].toObject().value( "life" ).toDouble(),
                   heroes[ i ].toObject().value( "gold" ).toDouble() );
    }

    emit GameBoard(jObj.value("game").toObject().value("board").toObject().value("size").toDouble(), jObj.value("game").toObject().value("board").toObject().value("tiles").toString());

    emit DataReceived();
}
