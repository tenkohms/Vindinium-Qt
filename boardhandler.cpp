#include "boardhandler.h"
#include <QList>
#include <QDebug>

BoardHandler::BoardHandler(QObject *parent)
    : QObject(parent)
    , size_ (0)
{
    boardMap_.clear();
}

void BoardHandler::SetBoard(int Size, QString Tiles, int playerID, int HaiID)
{
    playerID_ = playerID;

    if (HaiID != -1)
    {
        haiid_ = HaiID;
    }
    else
    {
        haiid_ = 0;
    }
    size_ = Size;
    ParseMap(Tiles);
}

QList<QPair<int, int> > BoardHandler::EnemyLocations()
{
    return boardMap_.keys( ENEMY );
}

QList<QPair<int, int> > BoardHandler::Taverns()
{
    return boardMap_.keys( TAVERN );
}

QList<QPair<int, int> > BoardHandler::MinesOfInterest()
{
    QList< QPair < int, int > > retLocations;
    retLocations += boardMap_.keys( ENEMYMINE );
    retLocations += boardMap_.keys( NEUTRALMINE );

    return retLocations;
}

int BoardHandler::PlayerOwnedMineCount()
{
    return boardMap_.keys( PLAYERMINE ).size();
}

QPair<int, int> BoardHandler::PlayerLocation()
{
    return boardMap_.key( PLAYER );
}

int BoardHandler::TileToInt(QString tile)
{
    /*
     * 0 = passable
     * 1 = impassable
     * 2 = player
     * 3 = enemy
     * 4 = tavern
     * 5 = neutral mine
     * 6 = player mine
     * 7 = enemy mine
     */

    if ( tile[0] == " " )
        return 0;

    if ( tile[0] == "#" )
        return 1;

    if ( tile[0] == "@" )
    {
        if ( tile[1] == QString::number(playerID_) )
            return 2;

        if ( tile[1] == QString::number(haiid_) )
            return 8;

        return 3;
    }

    if ( tile[0] == "[" )
        return 4;

    if ( tile[0] == "$" )
    {

        if ( tile[1] == "-" )
            return 5;

        if ( tile[1] == QString::number(playerID_) )
            return 6;

        return 7;
    }

    return 1;
}

int BoardHandler::TileToInt(Tile tile)
{
    /*
     * 0 = passable
     * 1 = impassable
     * 2 = player
     * 3 = enemy
     * 4 = tavern
     * 5 = neutral mine
     * 6 = player mine
     * 7 = enemy mine
     */

    if ( tile == PASSABLE )
        return 0;

    if ( tile == IMPASSABLE )
        return 1;

    if ( tile == PLAYER )
        return 2;

    if ( tile == ENEMY )
        return 3;

    if ( tile == TAVERN )
        return 4;

    if ( tile == NEUTRALMINE )
        return 5;

    if ( tile == PLAYERMINE )
        return 6;

    if ( tile == ENEMYMINE )
        return 7;

    return 1;
}

void BoardHandler::ParseMap(QString & Map)
{
    int counter = 0;
    for (int x = 0; x < size_; x++)
    {
        for (int y = 0; y < size_; y++)
        {
            QPair<int, int> pair;
            pair.first = x;
            pair.second = y;
            Tile tile = IMPASSABLE;
            //qDebug() << "(" << x << "," << y << "): " << Map.mid(counter, 2);
            QString tilePiece = Map.mid(counter, 2);
            counter += 2;


            /*
             * 0 = passable
             * 1 = impassable
             * 2 = player
             * 3 = enemy
             * 4 = tavern
             * 5 = neutral mine
             * 6 = player mine
             * 7 = enemy mine
             */
            switch ( TileToInt( tilePiece ) ) {
            case 0:
                tile = PASSABLE;
                break;

            case 1:
                tile = IMPASSABLE;
                break;

            case 2:
                tile = PLAYER;
                break;

            case 3:
                tile = ENEMY;
                break;

            case 4:
                tile = TAVERN;
                break;

            case 5:
                tile = NEUTRALMINE;
                break;

            case 6:
                tile = PLAYERMINE;
                break;

            case 7:
                tile = ENEMYMINE;
                break;

            case 8:
                tile = HAI;
                break;


            default:
                tile = IMPASSABLE;
                break;
            }

            boardMap_[pair] = tile;
        }
    }

    /*** Print Map ***/

#ifdef SHOWDEBUG
    for (int x = 0; x < size_; x++)
    {
        QString row;
        for (int y = 0; y < size_; y++)
        {
            QPair<int, int> pair;
            pair.first = x;
            pair.second = y;
            switch ( TileToInt( boardMap_[pair] ) )
            {
            case 0:
                row += " ";
                break;

            case 1:
                row += "X";
                break;

            case 2:
                row += "S";
                break;

            case 3:
                row += "E";
                break;

            case 4:
                row += "T";
                break;

            case 5:
                row += "N";
                break;

            case 6:
                row += "P";
                break;

            case 7:
                row += "M";
                break;


            default:
                row += "X";
                break;
            }
        }
        qDebug() << row;
    }

    qDebug() << boardMap_.key( PLAYER );

#endif
    emit DoneProcessing();
    //Dir( boardMap_.key( PLAYER ), boardMap_.key( TAVERN ) );
}

int BoardHandler::Dir(const QPair< int, int > & Source,
                          const QPair< int, int > & Destination )
{

    QMap< QPair< int, int >, int > frontierMap;
    int currentCount = 0;
    frontierMap[ Destination ] = currentCount;
    currentCount++;

    int ret = GetMap(frontierMap, currentCount, Source);
#ifdef SHOWDEBUG
    qDebug() << "\nFrontier Map\n";

    for (int x = 0; x < size_; x++)
    {
        QString row;
        for (int y = 0; y < size_; y++)
        {
            QPair<int, int> pair;
            pair.first = x;
            pair.second = y;
            if ( frontierMap.contains( pair ) )
            {
                if ( frontierMap[ pair ] == -1 )
                    row += "p,";
                else
                    row += QString::number( frontierMap[ pair ] ) + ",";

            }
            else
            {
                row += "  ";
            }
        }
        qDebug() << row;
    }
#endif
    //qDebug() << "RET VAL= " << ret;
    int x = Source.first;
    int y = Source.second;

    /*
     * 0 = UP
     * 1 = DOWN
     * 2 = RIGHT
     * 3 = LEFT
     */


    if ( ( frontierMap.contains(QPair< int, int >( x - 1, y ) ) )  && ( frontierMap[ QPair< int, int >( x - 1, y ) ] == ret - 1  ) )
       return 0;

    if ( ( frontierMap.contains(QPair< int, int >( x + 1, y ) ) )  && ( frontierMap[ QPair< int, int >( x + 1, y ) ] == ret - 1  ) )
        return 1;

    if ( ( frontierMap.contains(QPair< int, int >( x, y + 1 ) ) )  && ( frontierMap[ QPair< int, int >( x , y + 1 ) ] == ret - 1  ) )
        return 2;

    if ( ( frontierMap.contains(QPair< int, int >( x , y - 1 ) ) ) && ( frontierMap[ QPair< int, int >( x , y - 1 ) ] == ret - 1  ) )
        return 3;

    if ( ( frontierMap.contains(QPair< int, int >( x - 1, y ) ) )  && ( frontierMap[ QPair< int, int >( x - 1, y ) ] == 0  ) )
       return 0;

    if ( ( frontierMap.contains(QPair< int, int >( x + 1, y ) ) )  && ( frontierMap[ QPair< int, int >( x + 1, y ) ] == 0 ) )
        return 1;

    if ( ( frontierMap.contains(QPair< int, int >( x, y + 1 ) ) )  && ( frontierMap[ QPair< int, int >( x , y + 1 ) ] == 0 ) )
        return 2;

    if ( ( frontierMap.contains(QPair< int, int >( x , y - 1 ) ) )  && ( frontierMap[ QPair< int, int >( x , y - 1 ) ] == 0 ) )
        return 3;


    return 4;
}

int BoardHandler::Steps(const QPair<int, int> & Source, const QPair<int, int> & Destination)
{
    QMap< QPair< int, int >, int > frontierMap;
    int currentCount = 0;
    frontierMap[ Destination ] = currentCount;
    currentCount++;

    int retVal = GetMap(frontierMap, currentCount, Source);
    return retVal;
}

int BoardHandler::GetMap( QMap < QPair <int, int > , int > &oldMap, int currentCount , const QPair < int, int > & Destination)
{
    QList < QPair< int, int > > currentCountList = oldMap.keys( currentCount - 1);
    bool continueProcessing = false;

    for (int counter = 0 ; counter < currentCountList.size() ; counter++ )
    {
        int x = currentCountList[counter].first;
        int y = currentCountList[counter].second;
        if ( y - 1 >= 0)
        {
            QPair< int, int > NextPair(QPair< int, int > ( x, y - 1));

            if ( NextPair == Destination )
            {
                oldMap[ NextPair ] = currentCount;
                continueProcessing = true;
                break;
            }

            if ( oldMap.contains( NextPair ))
            {

            }
            else {
                if ( boardMap_[ NextPair ] == PASSABLE)
                    oldMap[ NextPair ] = currentCount;
                else
                    oldMap[ NextPair ] = -1;
            }
        }

        if ( y + 1 < size_ )
        {
            QPair< int, int > NextPair(QPair< int, int > ( x, y + 1));

            if ( NextPair == Destination )
            {
                oldMap[ NextPair ] = currentCount;
                continueProcessing = true;
                break;
            }
            if ( oldMap.contains( NextPair ))
            {

            }
            else {
                if ( boardMap_[ NextPair ] == PASSABLE)
                    oldMap[ NextPair ] = currentCount;
                else
                    oldMap[ NextPair ] = -1;
            }
        }

        if ( x - 1 >= 0 )
        {
            QPair< int, int > NextPair(QPair< int, int > ( x - 1, y));

            if ( NextPair == Destination )
            {
                oldMap[ NextPair ] = currentCount;
                continueProcessing = true;
                break;
            }
            if ( oldMap.contains( NextPair ))
            {

            }
            else {
                if ( boardMap_[ NextPair ] == PASSABLE)
                    oldMap[ NextPair ] = currentCount;
                else
                    oldMap[ NextPair ] = -1;
            }
        }

        if ( x + 1 < size_ )
        {
            QPair< int, int > NextPair(QPair< int, int > ( x + 1, y));

            if ( NextPair == Destination )
            {
                oldMap[ NextPair ] = currentCount;
                continueProcessing = true;
                break;
            }
            if ( oldMap.contains( NextPair ))
            {

            }
            else {
                if ( boardMap_[ NextPair ] == PASSABLE)
                    oldMap[ NextPair ] = currentCount;
                else
                    oldMap[ NextPair ] = -1;
            }
        }
    }

    if (!continueProcessing && currentCount < 30)
    {
        currentCount++;
        return GetMap(oldMap, currentCount, Destination);
    }
    if (!continueProcessing && currentCount > 30)
    {
        return 99;
    }

    else
        return currentCount;
}
