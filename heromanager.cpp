#include "heromanager.h"

HeroManager::HeroManager(QObject *parent) : QObject(parent)
{

}

HeroManager::~HeroManager()
{
    if ( !heroMap_.isEmpty() )
    {
        for ( QMap < int, Hero * >::iterator it = heroMap_.begin(); it != heroMap_.end(); it++ )
        {
            Hero *oldHero = it.value();
            delete oldHero;
        }
        heroMap_.clear();
    }
}

void HeroManager::AddHero(int id, QString name, QPair<int, int> position, int life, int gold)
{
    if ( heroMap_.contains( id ) )
    {
        Hero * oldHero = heroMap_[ id ];
        delete oldHero;

    }

    Hero * newHero = new Hero( id, name, position, life, gold );
    heroMap_[ id ] = newHero;
}

int HeroManager::GetHeroHealth(QPair<int, int> heroPos)
{
    if ( !heroMap_.isEmpty() )
    {
        Hero * oldHero;
        for ( QMap < int, Hero * >::iterator it = heroMap_.begin(); it != heroMap_.end(); it++ )
        {
            oldHero = it.value();
            if ( oldHero->position() == heroPos )
                break;
        }

        return oldHero->life();
    }

    return 0;
}

int HeroManager::GetHeroMoney(QPair<int, int> heroPos)
{
    if ( !heroMap_.isEmpty() )
    {
        Hero * oldHero;
        for ( QMap < int, Hero * >::iterator it = heroMap_.begin(); it != heroMap_.end(); it++ )
        {
            oldHero = it.value();
            if ( oldHero->position() == heroPos )
                break;
        }

        return oldHero->gold();
    }

    return 0;
}

QString HeroManager::GetHeroName(QPair<int, int> heroPos)
{
    if ( !heroMap_.isEmpty() )
    {
        Hero * oldHero;
        for ( QMap < int, Hero * >::iterator it = heroMap_.begin(); it != heroMap_.end(); it++ )
        {
            oldHero = it.value();
            if ( oldHero->position() == heroPos )
                break;
        }

        return oldHero->name();
    }

    return "";
}
