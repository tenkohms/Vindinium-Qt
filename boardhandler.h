#ifndef BOARDHANDLER_H
#define BOARDHANDLER_H

#include <QObject>
#include <QMap>
#include <QPair>

//#define SHOWDEBUG 1

enum Tile { HAI, IMPASSABLE, PASSABLE, ENEMY, PLAYER, NEUTRALMINE, PLAYERMINE, ENEMYMINE, TAVERN };

class BoardHandler : public QObject
{
    Q_OBJECT
public:
    explicit BoardHandler(QObject *parent = 0);
    QString Board();
    int BoardSize();
    QList < QPair < int, int > > EnemyLocations();
    QList < QPair< int, int > > Taverns();
    QList < QPair< int, int > > MinesOfInterest();

    int PlayerOwnedMineCount();
    QPair< int, int > PlayerLocation();
    int Dir(const QPair< int, int> &, const QPair< int, int> &);
    int Steps(const QPair< int, int> &, const QPair< int, int> &);
signals:
    void DoneProcessing();

public slots:
    void SetBoard(int Size = 0, QString Tiles = "", int playerID= 0, int HaiID = -1);

protected:
    int TileToInt(QString);
    int TileToInt(Tile);
    void ParseMap(QString &);
    int GetMap( QMap < QPair < int, int > , int > &, int, const QPair <int, int>  &);
private:
    int size_;
    QMap< QPair< int, int >, Tile > boardMap_;
    int playerID_;
    int haiid_;
};

#endif // BOARDHANDLER_H
