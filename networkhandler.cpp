#include "networkhandler.h"
#include <QDebug>
#include <QUrlQuery>

NetworkHandler::NetworkHandler(QObject *parent)
    : QObject(parent)
{
    networkManager_ = new QNetworkAccessManager(this);
    connect(networkManager_, SIGNAL(finished(QNetworkReply*)), this, SLOT(NetworkReplyFinished(QNetworkReply*)));
}

NetworkHandler::~NetworkHandler()
{
    delete(networkManager_);
}

void NetworkHandler::SendNetworkRequest(QString RequestURL, QString Key)
{
    if (!RequestURL.isEmpty() && !Key.isEmpty())
    {
        QNetworkRequest request;
        request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        request.setUrl(QUrl(RequestURL));
        QUrlQuery postData;
        postData.addQueryItem("key", Key);
        networkManager_->post(request, postData.toString().toUtf8());
    }
}

void NetworkHandler::SendNetworkRequest(QString RequestURL, QString Key, QString Dir)
{
    if (!RequestURL.isEmpty() && !Key.isEmpty() && !Dir.isEmpty())
    {
        QNetworkRequest request;
        request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        request.setUrl(QUrl(RequestURL));
        QUrlQuery postData;
        postData.addQueryItem("key", Key);
        postData.addQueryItem("dir", Dir);
        networkManager_->post(request, postData.toString().toUtf8());
    }
}

void NetworkHandler::NetworkReplyFinished(QNetworkReply * NetworkReply)
{
    const QString data = QString(NetworkReply->readAll());
    emit NetworkData(&data);
}
