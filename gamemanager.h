#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <QObject>
#include <QPair>
#include <QTimer>

#include "jsonhandler.h"
#include "networkhandler.h"
#include "boardhandler.h"
#include "movemanager.h"
#include "heromanager.h"

class GameManager : public QObject
{
    Q_OBJECT
public:
    explicit GameManager(QString Key, QString Username, QObject *parent = 0);
    ~GameManager();

    void Start();
signals:

public slots:
    void SetPlayUrl(QString);
    void SetViewUrl(QString);
    void SetBoard(int, QString);
    void AddHero( int id, QString name, QPair< int, int > position, int life, int gold );
    void TimerTimeout();
    void SetTotalTurns(int);
    void SetCurrentTurns(int);
    void SetPlayerID(int);
    void SetHai(int);
    void NullData();

    void Go();

private:
    NetworkHandler *networkHandler_;
    JSONHandler *jsonHandler_;
    BoardHandler *boardHandler_;
    HeroManager *heroManager_;
    MoveManager *moveManager_;
    QString playURL_, viewURL_, key_;
    int totalTurns_;
    int currentTurn_;
    int playerID_;
    int HaiID_;

    QTimer *NullTimer;
};

#endif // GAMEMANAGER_H
