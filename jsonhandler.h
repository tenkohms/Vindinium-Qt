#ifndef JSONHANDLER_H
#define JSONHANDLER_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPair>

class JSONHandler : public QObject
{
    Q_OBJECT
public:
    explicit JSONHandler(QString Username, QObject *parent = 0);

signals:
    void PlayURL(QString);
    void ViewURL(QString);
    void GameBoard(int, QString);
    void TotalTurns(int);
    void CurrentTurnCount(int);
    void NewHero(int , QString, QPair< int, int >, int, int);
    void PlayerID(int);
    void ProtectPlayer(int);
    void NullData();
    void DataReceived();

public slots:
    void NewJSONData(const QString *);

private:
    QString username;
};

#endif // JSONHANDLER_H
