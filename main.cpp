#include <QCoreApplication>
#include <QCommandLineParser>

#include <iostream>
#include "gamemanager.h"

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "no" << std::endl;
        return 1;
    }
    else
    {
        std::cout << "Key: " << argv[1] << std::endl;
    }
    QCoreApplication a(argc, argv);
    GameManager gameManager( QString::fromStdString( argv[1] ),
                             QString::fromStdString( argv[2] ));
    gameManager.Start();

    return a.exec();
    //writteninbash
    //GameManager gameManager("7f5bd6gx");
    //sirstinksalot
    //GameManager gameManager("x6uxxpwl");
    //leeroy1030
    //GameManager gameManager("my8ppw6d");
    //gameManager.Start();
    //return a.exec();
}
