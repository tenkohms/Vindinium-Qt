#ifndef HEROMANAGER_H
#define HEROMANAGER_H

#include <QObject>
#include <QPair>
#include <QMap>

#include "hero.h"

class HeroManager : public QObject
{
    Q_OBJECT
public:
    explicit HeroManager(QObject *parent = 0);
    ~HeroManager();
    void AddHero(int id, QString name, QPair< int, int > position, int life, int gold);
    int GetHeroHealth( QPair< int, int > heroPos );
    int GetHeroMoney (QPair< int, int > heroPos );
    QString GetHeroName( QPair< int, int > heroPos );

signals:

public slots:

private:
    QMap < int, Hero * > heroMap_;
};

#endif // HEROMANAGER_H
