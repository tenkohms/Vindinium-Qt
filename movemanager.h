#ifndef MOVEMANAGER_H
#define MOVEMANAGER_H

#include <QObject>
#include "boardhandler.h"
#include "heromanager.h"

class MoveManager : public QObject
{
    Q_OBJECT
public:
    explicit MoveManager(BoardHandler *Board, HeroManager *hm, QObject *parent = 0);
    QString GetMove();

signals:
    void Ready();

public slots:
    void BoardReadyToProcess();

protected:
    int Distance( QPair< int, int >, QPair< int, int> );

private:
    BoardHandler * boardHandler_;
    HeroManager * heroManager_;
    QString nextMove_;
};

#endif // MOVEMANAGER_H
