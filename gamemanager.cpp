#include <QDesktopServices>
#include "gamemanager.h"
#include "movemanager.h"

GameManager::GameManager(QString Key, QString Username, QObject *parent)
    : QObject(parent)
    , playURL_(QString())
    , viewURL_(QString())
    , key_(Key)
    , playerID_(0)
    , HaiID_(-1)
{
    networkHandler_ = new NetworkHandler;
    jsonHandler_ = new JSONHandler(Username);
    boardHandler_ = new BoardHandler;
    heroManager_ = new HeroManager();
    moveManager_ = new MoveManager(boardHandler_, heroManager_);

    NullTimer = new QTimer();
    NullTimer->setSingleShot(true);
    connect(NullTimer,SIGNAL(timeout()), this, SLOT(TimerTimeout()));


    connect( networkHandler_, SIGNAL( NetworkData( const QString*)), jsonHandler_, SLOT(NewJSONData(const QString*)));
    connect(jsonHandler_, SIGNAL(NullData()), this, SLOT(NullData()));
    connect( jsonHandler_, SIGNAL( ViewURL( QString ) ), this, SLOT( SetViewUrl( QString ) ) );
    connect( jsonHandler_, SIGNAL( PlayURL( QString ) ), this, SLOT( SetPlayUrl( QString ) ) );
    connect (jsonHandler_, SIGNAL(CurrentTurnCount(int)), this, SLOT(SetCurrentTurns(int)) );
    connect(jsonHandler_, SIGNAL(TotalTurns(int)), this, SLOT(SetTotalTurns(int)));
    connect( jsonHandler_, SIGNAL(PlayerID(int)), this, SLOT(SetPlayerID(int)));
    connect (jsonHandler_, SIGNAL(ProtectPlayer(int)), this, SLOT(SetHai(int)));
    connect( jsonHandler_, SIGNAL( NewHero(int,QString,QPair<int,int>,int,int)), this, SLOT(AddHero(int,QString,QPair<int,int>,int,int)));
    connect( jsonHandler_, SIGNAL( GameBoard( int, QString ) ), this, SLOT( SetBoard( int, QString ) ) );
    connect( boardHandler_, SIGNAL( DoneProcessing() ), moveManager_, SLOT( BoardReadyToProcess() ) );

    connect(moveManager_, SIGNAL(Ready()), this, SLOT(Go()));

}

GameManager::~GameManager()
{
    delete networkHandler_;
    delete jsonHandler_;
    delete boardHandler_;
    delete moveManager_;
    delete NullTimer;
}

void GameManager::NullData()
{
    NullTimer->start(10000);
}

void GameManager::SetPlayUrl(QString PlayURL)
{
    if (playURL_ != PlayURL)
    {
        playURL_ = PlayURL;
    }
}

void GameManager::SetViewUrl(QString ViewURL)
{
    if (viewURL_ != ViewURL)
    {
        viewURL_ = ViewURL;
        qDebug() << viewURL_;
    }
}

void GameManager::SetPlayerID(int ID)
{
    playerID_ = ID;
}

void GameManager::SetHai(int haiid)
{
    HaiID_ = haiid;
}

void GameManager::SetBoard(int Size, QString Tiles)
{
    boardHandler_->SetBoard( Size, Tiles, playerID_, HaiID_);
}

void GameManager::AddHero(int id, QString name, QPair<int, int> position, int life, int gold)
{
    heroManager_->AddHero(id, name, position, life, gold);
}

void GameManager::Start()
{
    if (!key_.isEmpty())
        networkHandler_->SendNetworkRequest("http://vindinium.org/api/arena", key_);
        //networkHandler_->SendNetworkRequest("http://vindinium.org/api/training", key_);
}

void GameManager::TimerTimeout()
{
    if (!key_.isEmpty())
        networkHandler_->SendNetworkRequest("http://vindinium.org/api/arena", key_);
}

void GameManager::SetTotalTurns(int TT)
{
    totalTurns_ = TT;
}

void GameManager::SetCurrentTurns(int CT)
{
    currentTurn_ = CT;
}

void GameManager::Go()
{
    //qDebug() << "Move: " << moveManager_->GetMove();
    //moveTimer->stop();
    if (currentTurn_ < totalTurns_)
        networkHandler_->SendNetworkRequest(playURL_, key_, moveManager_->GetMove());
    else
        networkHandler_->SendNetworkRequest("http://vindinium.org/api/arena", key_);
    //moveTimer->start(850);
}
