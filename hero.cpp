#include "hero.h"

Hero::Hero(int id, QString name, QPair<int, int> position, int life, int gold, QObject *parent)
    : QObject(parent)
    , id_(id)
    , name_(name)
    , position_(position)
    , life_(life)
    , gold_(gold)
{

}

int Hero::id()
{
    return id_;
}

QString Hero::name()
{
    return name_;
}

QPair< int, int > Hero::position()
{
    return position_;
}

int Hero::life()
{
    return life_;
}

int Hero::gold()
{
    return gold_;
}
